import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.Base;
import utilities.Waiter;

import java.util.List;

public class CarvanaTest extends Base {

    @Test(testName = "Validate Carvana home page title and url", priority = 1)
    public void validateTitleAndUrl(){
        driver.get("https://www.carvana.com/");
        Assert.assertEquals(driver.getTitle(), "Carvana | Buy & Finance Used Cars Online | At Home Delivery");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/");
    }

    @Test (testName = "Validate the Carvana logo", priority = 2)
    public void validateCarvanaLogo(){
        driver.get("https://www.carvana.com/");
        WebElement logo = driver.findElement(By.xpath("//div[@data-qa = 'logo-wrapper']"));
        Assert.assertTrue(logo.isDisplayed());
    }


    @Test (testName = "Validate the main navigation section items", priority = 3)
    public void validateNavigationHeaderSections() {
        driver.get("https://www.carvana.com/");
        WebElement HowItWorks = driver.findElement(By.cssSelector("div[class='HowItWorksstyles__MenuWrapper-jox6t0-5 kQizBt']"));
        WebElement about = driver.findElement(By.cssSelector("div[class='Logo__LogoWrapper-sc-14r2405-0 fSZhVx']"));
        WebElement support = driver.findElement(By.cssSelector("div[class='Logo__LogoWrapper-sc-14r2405-0 fSZhVx']"));
        Waiter.pause(2);
        Assert.assertTrue(HowItWorks.isDisplayed());
        Assert.assertTrue(about.isDisplayed());
        Assert.assertTrue(support.isDisplayed());
        Waiter.pause(2);
    }

    @Test (testName = "Validate the sign in error message", priority = 4)
    public void validateInvalidCredentialsErrorMessage(){
        driver.get("https://www.carvana.com/");
        WebElement signInButton = driver.findElement(By.xpath("//div[@data-qa = 'desktop-wrapper']"));
        signInButton.click();
        WebElement emailInputBox = driver.findElement(By.id("usernameField"));
        emailInputBox.sendKeys("johndoe@gmail.com");
        WebElement passwordInputBox = driver.findElement(By.id("passwordField"));
        passwordInputBox.sendKeys("abcd1234");
        WebElement signInButton2 = driver.findElement(By.xpath("//button[@data-cv = 'sign-in-submit']"));
        signInButton2.click();
        WebElement errorMessage = driver.findElement(By.xpath("//div[@data-qa = 'error-message-container']"));
        System.out.println(errorMessage.getText());
        Assert.assertEquals(errorMessage.getText(), "Email address and/or password combination is incorrect" +
                "\nPlease try again or reset your password.");
    }

    @Test (testName = "Validate the search filter options and search button", priority = 5)
    public void validateSearchHeader(){
        driver.get("https://www.carvana.com/");
        WebElement searchHeaderLink = driver.findElement(By.xpath("(//a[@href ='/cars'])[2]"));
        searchHeaderLink.click();
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/");
        WebElement searchInputBox = driver.findElement(By.xpath("//input[@data-test = 'SearchBarInput']"));
        Assert.assertTrue(searchInputBox.isDisplayed());
        List<WebElement> filterOptionsButtons = driver.findElements(By.xpath("//button[@data-qa = 'drop-down-wrap']"));
        String[] expectedResults = {"PAYMENT & PRICE", "MAKE & MODEL","BODY TYPE","YEAR & MILEAGE","FEATURES", "MORE FILTERS"};
        for (int i = 0; i < filterOptionsButtons.size(); i++) {
            Assert.assertEquals(expectedResults[i], filterOptionsButtons.get(i).getText());
        }
        searchInputBox.sendKeys("Tesla");
        WebElement goButton = driver.findElement(By.xpath("//button[@data-qa = 'go-button']"));
        Assert.assertTrue(goButton.isDisplayed());
    }

    @Test(priority = 6, testName = "Validate the search result tiles")
    public void ValidateSearch2(){
        driver.get("https://www.carvana.com/");

        WebElement search = driver.findElement(By.xpath("(//a[@class='track-link event-link-button secondary-button'])"));
        search.click();

        Waiter.pause(10);
        WebElement searchBox = driver.findElement(By.xpath("//div[@role='combobox']/input"));
        searchBox.sendKeys("mercedes-benz");

        WebElement goButton = driver.findElement(By.xpath("(//button[@data-qa='go-button'])"));
        goButton.click();
        Waiter.pause(5);
        Assert.assertTrue(driver.getCurrentUrl().contains("mercedes-benz"));


        List<WebElement> locators = driver.findElements(By.xpath("(//div[@class='tk-shell'])"));

        for (int i = 1; i <= locators.size(); i++) {
            String pictureLocator = "(//picture[@class='vehicle-image'])[" + i + "]";
            WebElement picture = driver.findElement(By.xpath(pictureLocator));
            Assert.assertTrue(picture.isDisplayed());

            String heartLocator = "(//div[@class='favorite-vehicle variant'])[" + i + "]";
            WebElement heart = driver.findElement(By.xpath(heartLocator));
            Assert.assertTrue(heart.isDisplayed());

            String tileBodyLocator = "(//div[@class='tk-shell'])[" + i + "]";
            WebElement tileBody = driver.findElement(By.xpath(tileBodyLocator));
            Assert.assertTrue(tileBody.isDisplayed());

            String yearMakeModelLocator = "(//div[@class='year-make-experiment'])[" + i + "]";
            WebElement yearMakeModel = driver.findElement(By.xpath(yearMakeModelLocator));
            Assert.assertTrue(yearMakeModel.isDisplayed() && yearMakeModel.getText() != null);

            String inventoryLocator = "(//div[@class='inventory-type-experiment carvana-certified'])[" + i + "]";
            WebElement inventory = driver.findElement(By.xpath(inventoryLocator));
            Assert.assertTrue(inventory.isDisplayed() && inventory.getText() != null);

            String trimMileageLocator = "(//div[@class='trim-mileage'])[" + i + "]";
            WebElement trimMileage = driver.findElement(By.xpath(trimMileageLocator));
            Assert.assertTrue(trimMileage.isDisplayed() && trimMileage.getText() != null);

            String priceLocator = "(//div[@class='price-variant '])[1]";
            WebElement priceText = driver.findElement(By.xpath(priceLocator));
            Assert.assertTrue(Integer.parseInt( priceText.getText().replaceAll("[^0-9]", "")) > 0);

            String paymentLocator = "(//div[@class='monthly-payment'])[" + i + "]";
            WebElement payment = driver.findElement(By.xpath(paymentLocator));
            Assert.assertTrue(payment.isDisplayed() && payment.getText() != null);

            String downPaymentLocator = "(//div[@class='down-payment'])[" + i + "]";
            WebElement downPayment = driver.findElement(By.xpath(downPaymentLocator));
            Assert.assertTrue(downPayment.isDisplayed() && downPayment.getText() != null);

            String deliveryChipLocator = "(//span[@class='delivery'])[" + i + "]";
            WebElement deliveryChip = driver.findElement(By.xpath(deliveryChipLocator));
            Assert.assertTrue(deliveryChip.isDisplayed() && deliveryChip.getText() != null);

        }
    }
}